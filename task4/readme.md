# Task4 Jenkins and GitLab CI pipelines

## jenkins_pipeline with jenkins and nginx 

```
#!groovy

pipeline {
    agent {
        label "centos_ansible"
    }
    options {
        timeout(time: 150, unit: 'MINUTES')
    }
    environment {
        gitrepo="git@github.com:vdmaxi4/DevOps-Autumn-19-internal.git"
        dockerfilepath="task4/docker_jenkins"
        dockerimage="myjenkinsimage"
        dockerctname="myjenkins"
        dockerrepo="vdmaxi4/jenkins"
    }
    stages {
        stage('Downloading dockerfile') {
            steps {
                git credentialsId: 'git-ssh', url: "${gitrepo}"
            }
        }
        stage('Building dockerfile') {
            steps {
                sh '''
                cd ${dockerfilepath} && docker build -t ${dockerimage} ..
                docker run -d --rm -it --name ${dockerctname} -p 80:80 ${dockerimage}
                '''
            }
        }
        stage('Testing') {
            steps {
                sh 'curl -v telnet://127.0.0.1:80; echo $?'
             }
        }
        stage('Deploy') {
           steps {
                script {
                    withDockerRegistry([ credentialsId: 'dockerhub' ]) {
                    sh 'docker tag ${dockerimage} ${dockerrepo}:${dockerimage}_${BUILD_NUMBER}'
                    sh 'docker push ${dockerrepo}:${dockerimage}_${BUILD_NUMBER}'
                    sh 'docker kill $(docker ps -a -q -f name=${dockerctname})'
                    sh 'docker rmi ${dockerrepo}:${dockerimage}_${BUILD_NUMBER}'
                    }

                }
            }
        }
    }
post {
    always {
        deleteDir()
    }
    failure {
        echo 'This will run only if failed'
    }
}
}

```

## jenkins_pipeline with small java jar file

```
#!groovy

pipeline {
    agent {
        label "centos_ansible"
    }
    options {
        timeout(time: 150, unit: 'MINUTES')
    }
    environment {
        gitrepo="git@github.com:vdmaxi4/DevOps-Autumn-19-internal.git"
        dockerfilepath="task4/docker_java_app"
        dockerimage="myappimage"
        javaapp="myapp"
    }
    
    stages {
        stage('Downloading dockerfile') {
            steps {
                git credentialsId: "git-ssh", url: "${gitrepo}"
            }
        }
        stage('Building dockerfile') {
            steps {
                sh 'cd ${dockerfilepath} && docker build -t ${dockerimage} . '
            }
        }
        stage('Testing') {
            steps {
                sh 'docker run --rm --name test ${dockerimage} -jar ${javaapp}.jar; if [ $? -ne 0 ]; then exit 1; else docker rmi ${dockerimage}; fi'
            }
        }
    }
post {
    always {
        deleteDir()
    }
    failure {
        echo 'This will run only if failed'
    }
}
}


```

## GitLab pipeline for jenkins and nginx
```
image: docker:stable

stages:
  - build
  - upload

variables:
  DOCKERFILE: "task4/docker_jenkins"
  DOCKERIMAGE: "myjenkinsimage"
  DOCKERCTNAME: "myjenkins"
  DOCKERREPO: "vdmaxi4/jenkins"
  # https://forum.gitlab.com/t/docker-dind-stops-working-after-12-1-0-update/28664/4?u=asdfklgash
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""


services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

before_script:
  - docker info
  - docker login -u $HUB_REGISTRY_LOGIN -p $HUB_REGISTRY_PASSWORD

## build stage
build:
  stage: build
  script:
    - apk add --no-cache curl
    - cd $DOCKERFILE && docker build -t $DOCKERIMAGE .
    - docker run --rm -d --name $DOCKERCTNAME -p 80:80 $DOCKERIMAGE && sleep 30
    - curl -v telnet://docker:80
    - docker tag $DOCKERIMAGE $DOCKERREPO:$DOCKERIMAGE
    - docker push $DOCKERREPO:$DOCKERIMAGE
    - docker kill $(docker ps -a -q -f name=$DOCKERCTNAME)
    - docker rmi $DOCKERIMAGE

## testing stage - just for example
upload:
  stage: upload
  script:
    - apk add --no-cache curl
    - docker run --rm -d --name $DOCKERCTNAME -p 80:80 $DOCKERREPO:$DOCKERIMAGE && sleep 30
    - curl -v telnet://docker:80
    - docker kill $(docker ps -a -q -f name=$DOCKERCTNAME)
    - docker rmi $DOCKERREPO:$DOCKERIMAGE
```

## GitLab-CI with Java small APP
```
image: docker:stable

stages:
  - build

variables:
  DOCKERFILE: "task4/docker_java_app"
  DOCKERREPO: "vdmaxi4/jenkins"
  DOCKERIMAGE: "myappimage"
  JAVAAPP: "myapp"
  # https://forum.gitlab.com/t/docker-dind-stops-working-after-12-1-0-update/28664/4?u=asdfklgash
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

before_script:
  - docker info
  - docker login -u $HUB_REGISTRY_LOGIN -p $HUB_REGISTRY_PASSWORD

## build stage
build:
  stage: build
  script:
    - apk add --no-cache curl
    - cd $DOCKERFILE && docker build -t $DOCKERIMAGE .
    - docker run --rm --name test $DOCKERIMAGE -jar $JAVAAPP.jar; if [ $? -ne 0 ]; then exit 1; else docker rmi $DOCKERIMAGE; fi

```
