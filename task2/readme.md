# Task2 Docker with Jenkins

## Dockerfile
```
FROM ubuntu:latest
RUN apt-get update; apt-get install -y wget openjdk-8-jdk; \
 wget -O /opt/jenkins.war http://mirrors.jenkins.io/war-stable/latest/jenkins.war;
WORKDIR /opt
ENTRYPOINT [ "java", "-jar", "jenkins.war"]
CMD [ "HTTP_PORT=8080" ]
```

![Task2_1.png](./screens/Task2_1.png)
![Task2_2.png](./screens/Task2_2.png)

## Result
![Task2_3.png](./screens/Task2_3.png)

