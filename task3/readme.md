# Task3 Docker-compose

![Task3_1.png](./screens/Task3_1.png)

## docker-compose.yml
```
version: '3'

services:
    apache_img:
        container_name: haweb_apache_con
        build: ./apache
        expose:
            - 80
        networks:
            public_net:
                ipv4_address: 192.168.0.10

    nginx_img:
        container_name: haweb_nginx_con
        build: ./nginx
        expose:
            - 80
        networks:
            public_net:
                ipv4_address: 192.168.0.11

    haproxy_img:
        build: ./haproxy
        ports:
            - 80:80
        expose:
            - 80
        networks:
            public_net:
                ipv4_address: 192.168.0.33

networks:
    public_net:
        driver: bridge
        ipam:
            driver: default
            config:
                - subnet: 192.168.0.0/24
```

./apache/Dockerfile
```
FROM httpd:2.4
COPY index.html /usr/local/apache2/htdocs/index.html
```

./nginx/Dockerfile
```
FROM nginx
COPY index.html /usr/share/nginx/html/index.html
```

./haproxy/Dockerfile
```
FROM haproxy:1.7
COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg
```

./haproxy/haproxy.conf
```
global
    log /dev/log local0
    log localhost local1 notice
    maxconn 2000
    daemon

defaults
    log global
    mode http
    option httplog
    option dontlognull
    retries 3
    timeout connect 5000
    timeout client 50000
    timeout server 50000

frontend http-in
    bind *:80
    default_backend webservers

backend webservers
    stats enable
    stats auth admin:admin
    stats uri /haproxy?stats
    balance roundrobin
    #option httpchk
    option forwardfor
    option http-server-close
    server server1 192.168.0.10:80 check port 80
    server server2 192.168.0.11:80 check port 80
```

